<?php 

/**
 * Ebenyx Technologies - Ebenyx API
 * @author desireadje : ebt_api.php
 */

/**
 * Fonction permettant de récupérer le rôle d’un utilisateur dans un projet
 * @return int
 */
function _user_project_access_level() {
    // return get_enum_element( 'access_levels', current_user_get_access_level() );
    return current_user_get_access_level();
}

/**
 * Fonction permettant de récupérer l’utilisateur en fonction du projet et du rôle
 * @param $p_project_id
 * @param $p_access_level
 * @return int|mixed
 */
function project_get_user_project_and_access_level( $p_project_id, $p_access_level ) {
    // $t_users = user_get_unassigned_by_project_id( $p_project_id );
    $t_users = project_get_local_user_rows( $p_project_id );

    if (empty($t_users)) {
        return NO_USER;
    }
        
    foreach ($t_users as $t_user){
        $access_level = (int) $t_user["access_level"];
        if ($access_level == $p_access_level){
            return $t_user["user_id"];
        }
        continue;
    }
    return NO_USER;
}

/**
 * @param $user_get_row
 * @return string
 */
function format_user_info( $user_get_row ) {
    $t_user_acces_level = get_user_acces_level_label($user_get_row['access_level']);
    return $user_get_row['realname'] . " (" . $user_get_row['username'] . ")";
}

/**
 * @param $get_user_acces_level
 * @return string
 */
function get_user_acces_level_label($t_user_access_level) {
    return get_enum_element( 'access_levels', $t_user_access_level );
}

function _project_get_all_user_rows( $p_project_id = ALL_PROJECTS, $p_access_level = ANYBODY, $p_include_global_users = true ) {
    return project_get_all_user_rows( $p_project_id, $p_access_level, $p_include_global_users );
}


/**
 *
 * @param $t_issue
 * @param $t_summary
 * @param $g_hostname
 * @param $g_db_username
 * @param $g_db_password
 * @param $g_database_name
 */
function create_bug_sms($t_issue, $t_summary, $g_hostname, $g_db_username, $g_db_password, $g_database_name) {
    $t_issue = (object) $t_issue;

    $t_bug_id = $t_issue->id;
    // $t_bug = $t_bug = bug_get( $t_bug_id, true );

    $p_project = (object) $t_issue->project;
    // $p_category = (object) $t_issue->category;
    // $p_reporter = (object) $t_issue->reporter;

    // recuperation des information du projet (NB : Il serai possible que chaque projet aient son propre sender)
    $t_project_id = $p_project->id;
    $t_project_name = $p_project->name;

    // Return an array of info about users who have access to the the given project
    $t_users = _project_get_all_user_rows( $t_project_id, ANYBODY, false );
    if ( empty($t_users) ){
        return;
    }

    // recuperation du sender
    $app_config = get_app_config();
    if ( is_null($app_config) ){
        return;
    }

    $t_sender = $app_config->sender;
    if ( is_blank($t_sender) ){
        return;
    }

    // formatage du SMS.
    $t_message = sprintf("%s - %s", $t_project_name, $t_summary);
    $t_submitted = submitted;

    foreach ($t_users as $p_key) {
        if ( is_blank( $p_key['contact'] ) ) {
            continue;
        }
        $t_destinataire_id = $p_key['id'];
        $t_numero = $p_key['contact'];

        // _sms_create( $t_bug_id, $t_project_id, $t_destinataire_id, $t_numero, $t_sender, $t_message, false, $t_submitted );
        _sms_create( $t_bug_id, $t_project_id, $t_destinataire_id, $t_numero, $t_sender, 0, false, $t_message );
    }
}


/**
 *
 * @param $t_bug_id
 * @param $t_project_id
 * @param $t_destinataire_id
 * @param $t_numero
 * @param $t_sender
 * @param string $t_message
 * @param bool $expired
 * @param string $t_statut_delivered
 * @return string
 * @throws \Mantis\Exceptions\ClientException
 */
// id, bug_id, project_id, destinataire_id, numero, sender, reference_sms, resource_id, accuse_reception, date_accuse_reception, status, statut_delivered, expired, date_created, message, last_modified
// bug_id, project_id, destinataire_id, numero, sender, status, expired, message, date_created, last_modified
function _sms_create( $t_bug_id,
                      $t_project_id,
                      $t_destinataire_id,
                      $t_numero,
                      $t_sender,
                      $t_status = 0,
                      $t_expired = false,
                      $t_message = ''
) {
    $t_cookie_string = auth_generate_unique_cookie_string();

    db_param_push();
    $t_query = 'INSERT INTO {bug_sms} ( bug_id,
                                        project_id,
                                        destinataire_id,
                                        numero,
                                        sender,
                                        status,
                                        expired,
                                        message,
                                        date_created,
                                        last_modified
                                     )
                              VALUES ( 
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() . ',
                                        ' . db_param() .
                                    ')';

    $t_db_query = db_query( $t_query,
        array(  $t_bug_id,              // bug_id
                $t_project_id,          // project_id
                $t_destinataire_id,     // destinataire_id
                $t_numero,              // numero
                $t_sender,              // sender
                $t_status,              // status
                $t_expired,             // expired
                $t_message,             // message
                db_now(),               // date_created
                db_now()                // last_modified
        )
    );

    # Create preferences for the bug_sms
    $t_sms_id = db_insert_id( db_get_table( 'bug_sms' ) );

    return $t_cookie_string;
}


/**
 * @return mixed
 */
function get_app_config(){
    $t_query = new DbQuery();
    $t_query->sql( 'SELECT * FROM {app_config} WHERE id = 1' );
    $t_query->execute();
    $result = $t_query->fetch();

    return $result==false?null:(object)$result;
}

/**
 * @return mixed
 */
function update_app_config($p_payload){
    $t_columns = '';
    foreach( array_keys( $p_payload ) as $t_col ) {
        $t_columns .= "\n\t\t$t_col = :$t_col,";
    }

    $t_query = new DbQuery( 'UPDATE {app_config} SET'
        . rtrim( $t_columns, ',' ) . '
        WHERE id = 1'
    );

    $t_query->execute( $p_payload );

    /*
        // exemple update_app_config
        $t_param = array(
            'name' => $p_name,
            'status' => (int)$p_status,
            'enabled' => $c_enabled,
            'view_state' => (int)$p_view_state,
            'file_path' => $p_file_path,
            'description' => $p_description,
            'inherit_global' => $c_inherit_global,
        );
        $t_columns = '';
        foreach( array_keys( $t_param ) as $t_col ) {
            $t_columns .= "\n\t\t$t_col = :$t_col,";
        }

        $t_param['project_id'] = $p_project_id;
        $t_query = new DbQuery( 'UPDATE {project} SET'
            . rtrim( $t_columns, ',' ) . '
            WHERE id = :project_id'
        );
        $t_query->execute( $t_param );
        // exemple update_app_config
    */
}





?>