<?php
/**
 * Columns include file
 * @package MantisBT
 * @copyright Copyright 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
 * @copyright Copyright 2002  MantisBT Team - mantisbt-dev@lists.sourceforge.net
 * @link http://www.mantisbt.org
 *
 * @uses access_api.php
 * @uses authentication_api.php
 * @uses current_user_api.php
 * @uses file_api.php
 * @uses form_api.php
 * @uses helper_api.php
 * @uses html_api.php
 * @uses lang_api.php
 * @uses print_api.php
 */

require_api( 'access_api.php' );
require_api( 'authentication_api.php' );
require_api( 'current_user_api.php' );
require_api( 'file_api.php' );
require_api( 'form_api.php' );
require_api( 'helper_api.php' );
require_api( 'html_api.php' );
require_api( 'lang_api.php' );
require_api( 'print_api.php' );
// ebenxy
include("ebenyx/ebt_api.php");
// ebenxy

$t_app_config  = get_app_config();
$t_date_format = config_get( 'normal_date_format' );

?>
<div class="col-md-12 col-xs-12">
    <div class="space-10"></div>
    <form id="manage-columns-form" method="post" action="manage_config_api_sms_set.php">

    	<?= form_security_field( 'manage_config_api_sms_set' ) ?>

	    <div class="widget-box widget-color-blue2">
	        <div class="widget-header widget-header-small">
	            <h4 class="widget-title lighter">
	                <?php print_icon( 'fa-rss', 'ace-icon' ); ?>
	                <?= lang_get( 'param_api_sms_config' ) ?>
	            </h4>
	        </div>
	        <div id="manage-columns-div" class="form-container">
	                <div class="widget-body">
	                    <div class="widget-main no-padding">
	                        <div class="table-responsive">
	                            <table class="table table-bordered table-condensed table-striped">
	                                <fieldset>

	                                    <input type="hidden" name="id" value="<?= $t_app_config->id ?>" readonly="<?= $t_app_config->id ?>" required="">
	                                    
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'api_sms_app_base_url' )?>
	                                            <span class="required">*</span>
	                                        </td>
	                                        <td>
	                                            <input class="form-control" type="url" id="base_url" name="base_url" placeholder="<?= lang_get( 'api_sms_app_name' )?>" value="<?= $t_app_config->base_url ?>" required="">
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'api_sms_app_name' )?>
	                                            <span class="required">*</span>
	                                        </td>
	                                        <td>
	                                            <textarea class="form-control" id="titre" <?= helper_get_tab_index() ?> name="titre" cols="80" rows="2" placeholder="<?= lang_get( 'api_sms_app_name' )?>" required=""><?= $t_app_config->titre ?></textarea>
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'username' )?>
	                                            <span class="required">*</span>
	                                        </td>
	                                        <td>
	                                            <input class="form-control" type="text" id="username" name="username" placeholder="<?= lang_get( 'username' )?>" value="<?= $t_app_config->username ?>" required="">
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'password' )?>
	                                            <span class="required">*</span>
	                                        </td>
	                                        <td>
	                                            <input class="form-control" type="text" id="password" name="password" placeholder="<?= lang_get( 'password' )?>" value="<?= $t_app_config->password ?>" required="">
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'api_sms_app_sender' )?>
	                                            <span class="required">*</span>
	                                        </td>
	                                        <td>
	                                            <input class="form-control" type="text" id="sender" name="sender" placeholder="<?= lang_get( 'api_sms_app_sender' )?>" value="<?= $t_app_config->sender ?>" required="">
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'api_tokens_link' )?>
	                                        </td>
	                                        <td>
	                                            <textarea class="form-control" id="token" <?= helper_get_tab_index() ?> name="token" cols="80" rows="2" placeholder="<?= lang_get( 'api_tokens_link' )?>" readonly="<?= $t_app_config->token ?>" disabled><?= $t_app_config->token ?></textarea>
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'api_sms_app_token_exprire_date' )?>
	                                        </td>
	                                        <td>
	                                            <?php printf(date( $t_date_format, $t_app_config->token_exprire )); ?>
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td class="category">
	                                            <?= lang_get( 'last_update' )?>
	                                        </td>
	                                        <td>
	                                            <?php printf(date( $t_date_format, $t_app_config->last_modified )); ?>
	                                        </td>
	                                    </tr>
	                                </fieldset>
	                            </table>
	                        </div>
	                    </div>
	                    <div class="widget-toolbox padding-8 clearfix">
	                        <span class="required pull-right"> * <?= lang_get( 'required' ); ?></span>

	                        <input type="submit" name="save_settings_button" class="btn btn-primary btn-white btn-round btn-xs" value="<?= lang_get( 'save_settings_button' ) ?>" />
	                    </div>
	                </div>
	        </div>
	    </div>

	    <div class="space-10"></div>
    </form>
<?php echo '</div>';