<?php

/**
 * Set Columns Configuration
 *
 * @package MantisBT
 * @copyright Copyright 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
 * @copyright Copyright 2002  MantisBT Team - mantisbt-dev@lists.sourceforge.net
 * @link http://www.mantisbt.org
 *
 * @uses core.php
 * @uses access_api.php
 * @uses authentication_api.php
 * @uses columns_api.php
 * @uses config_api.php
 * @uses constant_inc.php
 * @uses current_user_api.php
 * @uses form_api.php
 * @uses gpc_api.php
 * @uses html_api.php
 * @uses lang_api.php
 * @uses print_api.php
 * @uses project_api.php
 */
require_once( 'core.php' );
require_api( 'access_api.php' );
require_api( 'authentication_api.php' );
require_api( 'current_user_api.php' );
require_api( 'file_api.php' );
require_api( 'form_api.php' );
require_api( 'helper_api.php' );
require_api( 'html_api.php' );
require_api( 'lang_api.php' );
require_api( 'print_api.php' );
// ebenxy
include("ebenyx/ebt_api.php");
// ebenxy

form_security_validate( 'manage_config_api_sms_set' );

$f_param_id = gpc_get_int( 'id' );
$f_base_url = gpc_get_string( 'base_url' );
$f_titre = gpc_get_string( 'titre' );
$f_username = gpc_get_string( 'username' );
$f_password = gpc_get_string( 'password' );
$f_sender = gpc_get_string( 'sender' );

// var_dump($f_sender);
// exit;

$t_payload = array(
	// 'id' => $f_param_id,
	'base_url' => $f_base_url,
	'titre' => $f_titre,
	'username' => $f_username,
	'password' => $f_password,
	'sender' => $f_sender,
	'run_get_token' => true,
	'last_modified' => db_now()
);

update_app_config($t_payload);


form_security_purge( 'manage_config_api_sms_set' );

$t_redirect_url = 'manage_config_api_sms_page.php';

layout_page_header();

layout_page_begin();

html_operation_successful( $t_redirect_url );

layout_page_end();
