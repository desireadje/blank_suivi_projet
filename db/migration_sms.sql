-- Migration pour SMS & WHATSAPP
-- version 23_06_2022
-- author : desireadje


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Structure de la table `user_table`
--
ALTER TABLE `user_table` ADD `contact` VARCHAR(64) NULL DEFAULT NULL COMMENT 'numero_de_telephone' AFTER `password`;
ALTER TABLE `user_table` ADD `numero_whatsapp` VARCHAR(64) NULL DEFAULT NULL COMMENT 'numero_whatsapp' AFTER `contact`;



-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --



--
-- Structure de la table `bug_sms_table`
--
CREATE TABLE `bug_sms_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `destinataire_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `numero` varchar(30) NOT NULL DEFAULT '',
  `sender` varchar(30) NOT NULL DEFAULT '',
  `reference_sms` varchar(50) NOT NULL DEFAULT '',
  `resource_id` varchar(50) NOT NULL DEFAULT '',
  `accuse_reception` boolean DEFAULT false,
  `date_accuse_reception` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` smallint(1) NOT NULL DEFAULT 0,
  `statut_delivered` varchar(50) NOT NULL DEFAULT '',
  `date_recieve_sms` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `expired` boolean DEFAULT false,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `message` longtext NOT NULL DEFAULT '',
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexs pour la table `bug_sms_table`
--
ALTER TABLE `bug_sms_table` ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour la table `bug_sms_table`
--
ALTER TABLE `bug_sms_table` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;



-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --



--
-- Structure de la table `app_config_table`
--

CREATE TABLE `app_config_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `base_url` varchar(255) NOT NULL DEFAULT 'https://localhost:8080',
  `titre` varchar(255) NOT NULL DEFAULT 'Ebenyx - Suivi de projets',
  `username` varchar(30) NOT NULL DEFAULT 'suiviprojet',
  `password` varchar(255) NOT NULL DEFAULT 'EbenyxMdp2022$',
  `sender` varchar(255) NOT NULL DEFAULT 'SUIVI-PROJET',
  `token` varchar(255) NOT NULL DEFAULT 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWl2aXByb2pldCIsImV4cCI6MTY1NDY0NTQwMiwiaWF0IjoxNjU0NjI3NDAyfQ.6spNoqO7d6RiPZb7kQtJlgUHvbPh6uCM_jZUE6BMHgd5Oe6WyYE_zpULhL0R9lcBxDjEtRRiJU8eltTh27I2EA',
  `token_exprire` int(10) UNSIGNED NOT NULL DEFAULT 1656010959,
  `run_get_token` tinyint(1) DEFAULT 0,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1656010959
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Index pour la table `app_config_table`
--
ALTER TABLE `app_config_table` ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour la table `app_config_table`
--
ALTER TABLE `app_config_table` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


--
-- Déchargement des données de la table `app_config_table`
--

INSERT INTO `app_config_table` (`id`, `base_url`, `titre`, `username`, `password`, `sender`, `token`, `run_get_token`, `token_exprire`, `last_modified`) VALUES
(1, 'https://localhost:8080', 'Ebenyx - Suivi de projets', 'suiviprojet', 'EbenyxMdp2022$', 'SUIVI-PROJET', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWl2aXByb2pldCIsImV4cCI6MTY1NDY0NTQwMiwiaWF0IjoxNjU0NjI3NDAyfQ.6spNoqO7d6RiPZb7kQtJlgUHvbPh6uCM_jZUE6BMHgd5Oe6WyYE_zpULhL0R9lcBxDjEtRRiJU8eltTh27I2EA', 0, 1656010959, 1656010959);

COMMIT;

