-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 21 oct. 2022 à 13:07
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `api_token_table`
--

CREATE TABLE `api_token_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL,
  `hash` varchar(128) NOT NULL,
  `plain_token` varchar(128) NOT NULL COMMENT '// ebenyx Token authorisation',
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_used` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `api_token_table`
--

INSERT INTO `api_token_table` (`id`, `user_id`, `name`, `hash`, `plain_token`, `date_created`, `date_used`) VALUES
(1, 1, 'administrator', '275051b69a26ba09264e92dc1a0c5be8d2bae262ab5f304d93569c1d321509ed', 'Mt0Aa3ZPOrWis0DZR0iD3MNKQ6r0D9Sr', 1643886947, 1);

-- --------------------------------------------------------

--
-- Structure de la table `app_config_table`
--

CREATE TABLE `app_config_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `base_url` varchar(255) NOT NULL DEFAULT 'https://localhost:8080',
  `titre` varchar(255) NOT NULL DEFAULT 'Ebenyx - Suivi de projets',
  `username` varchar(30) NOT NULL DEFAULT 'suiviprojet',
  `password` varchar(255) NOT NULL DEFAULT 'EbenyxMdp2022$',
  `sender` varchar(255) NOT NULL DEFAULT 'SUIVI-PROJET',
  `token` varchar(255) NOT NULL DEFAULT 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWl2aXByb2pldCIsImV4cCI6MTY1NDY0NTQwMiwiaWF0IjoxNjU0NjI3NDAyfQ.6spNoqO7d6RiPZb7kQtJlgUHvbPh6uCM_jZUE6BMHgd5Oe6WyYE_zpULhL0R9lcBxDjEtRRiJU8eltTh27I2EA',
  `token_exprire` int(10) UNSIGNED NOT NULL DEFAULT 1656010959,
  `run_get_token` tinyint(1) DEFAULT 0,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1656010959
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `app_config_table`
--

INSERT INTO `app_config_table` (`id`, `base_url`, `titre`, `username`, `password`, `sender`, `token`, `token_exprire`, `run_get_token`, `last_modified`) VALUES
(1, 'https://localhost:8080', 'Ebenyx - Suivi de projets', 'suiviprojet', 'EbenyxMdp2022$', 'SUIVI-PROJET', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWl2aXByb2pldCIsImV4cCI6MTY1NDY0NTQwMiwiaWF0IjoxNjU0NjI3NDAyfQ.6spNoqO7d6RiPZb7kQtJlgUHvbPh6uCM_jZUE6BMHgd5Oe6WyYE_zpULhL0R9lcBxDjEtRRiJU8eltTh27I2EA', 1656010959, 0, 1656010959);

-- --------------------------------------------------------

--
-- Structure de la table `bugnote_table`
--

CREATE TABLE `bugnote_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reporter_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bugnote_text_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `note_type` int(11) DEFAULT 0,
  `note_attr` varchar(250) DEFAULT '',
  `time_tracking` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bugnote_text_table`
--

CREATE TABLE `bugnote_text_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `note` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_file_table`
--

CREATE TABLE `bug_file_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bugnote_id` int(10) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_history_table`
--

CREATE TABLE `bug_history_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `field_name` varchar(64) NOT NULL,
  `old_value` varchar(255) NOT NULL,
  `new_value` varchar(255) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT 0,
  `date_modified` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_monitor_table`
--

CREATE TABLE `bug_monitor_table` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bug_monitor_table`
--

INSERT INTO `bug_monitor_table` (`user_id`, `bug_id`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `bug_relationship_table`
--

CREATE TABLE `bug_relationship_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `source_bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `destination_bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `relationship_type` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_revision_table`
--

CREATE TABLE `bug_revision_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL,
  `bugnote_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_sms_table`
--

CREATE TABLE `bug_sms_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `destinataire_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `numero` varchar(30) NOT NULL DEFAULT '',
  `sender` varchar(30) NOT NULL DEFAULT '',
  `reference_sms` varchar(50) NOT NULL DEFAULT '',
  `resource_id` varchar(50) NOT NULL DEFAULT '',
  `accuse_reception` tinyint(1) DEFAULT 0,
  `date_accuse_reception` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` smallint(1) NOT NULL DEFAULT 0,
  `statut_delivered` varchar(50) NOT NULL DEFAULT '',
  `date_recieve_sms` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `expired` tinyint(1) DEFAULT 0,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `message` longtext NOT NULL DEFAULT '',
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_table`
--

CREATE TABLE `bug_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reporter_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `handler_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `duplicate_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `priority` smallint(6) NOT NULL DEFAULT 30,
  `severity` smallint(6) NOT NULL DEFAULT 50,
  `reproducibility` smallint(6) NOT NULL DEFAULT 10,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `resolution` smallint(6) NOT NULL DEFAULT 10,
  `projection` smallint(6) NOT NULL DEFAULT 10,
  `eta` smallint(6) NOT NULL DEFAULT 10,
  `bug_text_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `platform` varchar(32) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '',
  `fixed_in_version` varchar(64) NOT NULL DEFAULT '',
  `build` varchar(32) NOT NULL DEFAULT '',
  `profile_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `summary` varchar(128) NOT NULL DEFAULT '',
  `sponsorship_total` int(11) NOT NULL DEFAULT 0,
  `sticky` tinyint(4) NOT NULL DEFAULT 0,
  `target_version` varchar(64) NOT NULL DEFAULT '',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `due_date` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `last_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_tag_table`
--

CREATE TABLE `bug_tag_table` (
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tag_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `date_attached` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bug_text_table`
--

CREATE TABLE `bug_text_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext NOT NULL,
  `steps_to_reproduce` longtext NOT NULL,
  `additional_information` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `category_table`
--

CREATE TABLE `category_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `category_table`
--

INSERT INTO `category_table` (`id`, `project_id`, `user_id`, `name`, `status`) VALUES
(1, 0, 1, 'General', 0);

-- --------------------------------------------------------

--
-- Structure de la table `config_table`
--

CREATE TABLE `config_table` (
  `config_id` varchar(64) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `access_reqd` int(11) DEFAULT 0,
  `type` int(11) DEFAULT 90,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `config_table`
--

INSERT INTO `config_table` (`config_id`, `project_id`, `user_id`, `access_reqd`, `type`, `value`) VALUES
('allow_reporter_reopen', 0, 0, 90, 1, '0'),
('auto_set_status_to_assigned', 0, 0, 90, 1, '0'),
('bugnote_user_delete_threshold', 0, 0, 90, 1, '100'),
('bugnote_user_edit_threshold', 0, 0, 90, 1, '25'),
('csv_columns', 0, 0, 90, 3, '[\"id\",\"project_id\",\"reporter_id\",\"handler_id\",\"priority\",\"severity\",\"reproducibility\",\"version\",\"category_id\",\"date_submitted\",\"os\",\"os_build\",\"platform\",\"view_state\",\"last_updated\",\"summary\",\"status\",\"resolution\",\"fixed_in_version\"]'),
('database_version', 0, 0, 90, 1, '211'),
('default_notify_flags', 0, 0, 90, 3, '{\"category\":1,\"threshold_min\":100,\"threshold_max\":100}'),
('delete_bugnote_threshold', 0, 0, 90, 1, '100'),
('delete_bug_threshold', 0, 0, 90, 1, '100'),
('excel_columns', 0, 0, 90, 3, '[\"id\",\"project_id\",\"reporter_id\",\"handler_id\",\"priority\",\"severity\",\"reproducibility\",\"version\",\"category_id\",\"date_submitted\",\"os\",\"os_build\",\"platform\",\"view_state\",\"last_updated\",\"summary\",\"status\",\"resolution\",\"fixed_in_version\"]'),
('font_family', 0, 1, 90, 2, 'Poppins'),
('font_family', 0, 2, 90, 2, 'Poppins'),
('handle_bug_threshold', 0, 0, 90, 1, '40'),
('limit_view_unless_threshold', 0, 0, 90, 1, '10'),
('monitor_bug_threshold', 0, 0, 90, 1, '55'),
('move_bug_threshold', 0, 0, 90, 1, '100'),
('notify_flags', 0, 0, 90, 3, '{\"updated\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"owner\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"reopened\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"deleted\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"bugnote\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"relation\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"monitor\":{\"reporter\":0,\"handler\":0,\"monitor\":0,\"bugnotes\":0,\"category\":1,\"threshold_max\":0},\"new\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":0,\"category\":1,\"threshold_min\":\"25\",\"threshold_max\":\"90\"},\"feedback\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_min\":\"25\",\"threshold_max\":\"25\"},\"acknowledged\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"confirmed\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"assigned\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"resolved\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0},\"closed\":{\"reporter\":1,\"handler\":1,\"monitor\":1,\"bugnotes\":1,\"category\":1,\"threshold_max\":0}}'),
('plugin_Gravatar_schema', 0, 0, 90, 1, '-1'),
('plugin_MantisGraph_schema', 0, 0, 90, 1, '-1'),
('plugin_XmlImportExport_schema', 0, 0, 90, 1, '-1'),
('print_issues_page_columns', 0, 0, 90, 3, '[\"selection\",\"priority\",\"id\",\"bugnotes_count\",\"attachment_count\",\"category_id\",\"severity\",\"status\",\"last_updated\",\"summary\"]'),
('private_bugnote_threshold', 0, 0, 90, 3, '[10,40,55,70,90]'),
('private_bug_threshold', 0, 0, 90, 3, '[10,55,70,90]'),
('reopen_bug_threshold', 0, 0, 90, 1, '90'),
('report_bug_threshold', 0, 0, 90, 3, '[25,70,90]'),
('show_monitor_list_threshold', 0, 0, 90, 3, '[10,55,70,90]'),
('status_enum_workflow', 0, 0, 90, 3, '{\"10\":\"20:en attente,40:confirm\\u00e9,90:ferm\\u00e9\",\"20\":\"40:confirm\\u00e9,90:ferm\\u00e9\",\"30\":\"\",\"40\":\"80:trait\\u00e9\",\"50\":\"\",\"80\":\"20:en attente,90:ferm\\u00e9\",\"90\":\"20:en attente\"}'),
('update_bugnote_threshold', 0, 0, 90, 1, '100'),
('update_bug_assign_threshold', 0, 0, 90, 1, '70'),
('update_bug_threshold', 0, 0, 90, 1, '55'),
('view_issues_page_columns', 0, 0, 90, 3, '[\"selection\",\"priority\",\"id\",\"bugnotes_count\",\"attachment_count\",\"severity\",\"last_updated\",\"summary\",\"status\",\"resolution\",\"project_id\",\"reporter_id\"]'),
('view_issues_page_columns', 2, 0, 90, 3, '[\"selection\",\"priority\",\"id\",\"bugnotes_count\",\"attachment_count\",\"severity\",\"last_updated\",\"summary\",\"status\",\"resolution\",\"project_id\",\"reporter_id\"]'),
('view_summary_threshold', 0, 0, 90, 3, '[10,70,90]');

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_project_table`
--

CREATE TABLE `custom_field_project_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sequence` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_string_table`
--

CREATE TABLE `custom_field_string_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `value` varchar(255) NOT NULL DEFAULT '',
  `text` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_table`
--

CREATE TABLE `custom_field_table` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT 0,
  `possible_values` text DEFAULT NULL,
  `default_value` varchar(255) NOT NULL DEFAULT '',
  `valid_regexp` varchar(255) NOT NULL DEFAULT '',
  `access_level_r` smallint(6) NOT NULL DEFAULT 0,
  `access_level_rw` smallint(6) NOT NULL DEFAULT 0,
  `length_min` int(11) NOT NULL DEFAULT 0,
  `length_max` int(11) NOT NULL DEFAULT 0,
  `require_report` tinyint(4) NOT NULL DEFAULT 0,
  `require_update` tinyint(4) NOT NULL DEFAULT 0,
  `display_report` tinyint(4) NOT NULL DEFAULT 0,
  `display_update` tinyint(4) NOT NULL DEFAULT 1,
  `require_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_closed` tinyint(4) NOT NULL DEFAULT 0,
  `require_closed` tinyint(4) NOT NULL DEFAULT 0,
  `filter_by` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `email_table`
--

CREATE TABLE `email_table` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) NOT NULL DEFAULT '',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `metadata` longtext NOT NULL,
  `body` longtext NOT NULL,
  `submitted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `filters_table`
--

CREATE TABLE `filters_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `is_public` tinyint(4) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `filter_string` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `filters_table`
--

INSERT INTO `filters_table` (`id`, `user_id`, `project_id`, `is_public`, `name`, `filter_string`) VALUES
(1, 2, -1, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"per_page\":50,\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"sort\":\"priority,last_updated\",\"dir\":\"ASC,DESC\",\"filter_by_date\":false,\"start_month\":1,\"start_day\":1,\"start_year\":2022,\"end_month\":1,\"end_day\":6,\"end_year\":2022,\"filter_by_last_updated_date\":false,\"last_updated_start_month\":1,\"last_updated_start_day\":1,\"last_updated_start_year\":2022,\"last_updated_end_month\":1,\"last_updated_end_day\":6,\"last_updated_end_year\":2022,\"search\":\"\",\"hide_status\":[90],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"priority\":[0],\"monitor_user_id\":[0],\"view_state\":0,\"custom_fields\":[],\"sticky\":true,\"relationship_type\":-1,\"relationship_bug\":0,\"profile_id\":[0],\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"tag_string\":\"\",\"tag_select\":0,\"note_user_id\":[0],\"match_type\":0}'),
(2, 3, -2, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"01\",\"end_month\":\"01\",\"start_day\":1,\"end_day\":\"06\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"01\",\"last_updated_end_month\":\"01\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"06\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(3, 3, -1, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"01\",\"end_month\":\"01\",\"start_day\":1,\"end_day\":\"06\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"01\",\"last_updated_end_month\":\"01\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"06\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":3}'),
(4, 3, 0, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"01\",\"end_month\":\"01\",\"start_day\":1,\"end_day\":\"06\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"01\",\"last_updated_end_month\":\"01\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"06\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(5, 8, 0, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"per_page\":50,\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"sort\":\"last_updated,reporter_id,status,resolution,project_id\",\"dir\":\"DESC,ASC,ASC,ASC,ASC\",\"filter_by_date\":false,\"start_month\":3,\"start_day\":1,\"start_year\":2022,\"end_month\":3,\"end_day\":7,\"end_year\":2022,\"filter_by_last_updated_date\":false,\"last_updated_start_month\":3,\"last_updated_start_day\":1,\"last_updated_start_year\":2022,\"last_updated_end_month\":3,\"last_updated_end_day\":7,\"last_updated_end_year\":2022,\"search\":\"\",\"hide_status\":[90],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"priority\":[0],\"monitor_user_id\":[0],\"view_state\":0,\"custom_fields\":[],\"sticky\":true,\"relationship_type\":-1,\"relationship_bug\":0,\"profile_id\":[0],\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"tag_string\":\"\",\"tag_select\":0,\"note_user_id\":[0],\"match_type\":0}'),
(6, 8, -2, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"03\",\"end_month\":\"03\",\"start_day\":1,\"end_day\":\"07\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"03\",\"last_updated_end_month\":\"03\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"07\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":6}'),
(7, 9, -4, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"per_page\":50,\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"sort\":\"priority,last_updated\",\"dir\":\"DESC,DESC\",\"filter_by_date\":false,\"start_month\":3,\"start_day\":1,\"start_year\":2022,\"end_month\":3,\"end_day\":16,\"end_year\":2022,\"filter_by_last_updated_date\":false,\"last_updated_start_month\":3,\"last_updated_start_day\":1,\"last_updated_start_year\":2022,\"last_updated_end_month\":3,\"last_updated_end_day\":16,\"last_updated_end_year\":2022,\"search\":\"\",\"hide_status\":[90],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"priority\":[0],\"monitor_user_id\":[0],\"view_state\":0,\"custom_fields\":[],\"sticky\":true,\"relationship_type\":-1,\"relationship_bug\":0,\"profile_id\":[0],\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"tag_string\":\"\",\"tag_select\":0,\"note_user_id\":[0],\"match_type\":0}'),
(8, 2, 0, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"04\",\"end_month\":\"04\",\"start_day\":1,\"end_day\":\"21\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"04\",\"last_updated_end_month\":\"04\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"21\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(9, 4, 0, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"04\",\"end_month\":\"04\",\"start_day\":1,\"end_day\":\"21\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"04\",\"last_updated_end_month\":\"04\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"21\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(10, 8, -7, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"05\",\"end_month\":\"05\",\"start_day\":1,\"end_day\":\"04\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"05\",\"last_updated_end_month\":\"05\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"04\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":10}'),
(11, 8, -12, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"05\",\"end_month\":\"05\",\"start_day\":1,\"end_day\":\"09\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"05\",\"last_updated_end_month\":\"05\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"09\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(12, 8, -13, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"05\",\"end_month\":\"05\",\"start_day\":1,\"end_day\":\"12\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"05\",\"last_updated_end_month\":\"05\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"12\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(13, 8, -1, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"05\",\"end_month\":\"05\",\"start_day\":1,\"end_day\":\"23\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"05\",\"last_updated_end_month\":\"05\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"23\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(14, 8, -10, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"06\",\"end_month\":\"06\",\"start_day\":1,\"end_day\":\"07\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"06\",\"last_updated_end_month\":\"06\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"07\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":14}'),
(15, 8, -17, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"07\",\"end_month\":\"07\",\"start_day\":1,\"end_day\":\"08\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"07\",\"last_updated_end_month\":\"07\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"08\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(16, 8, -19, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"08\",\"end_month\":\"08\",\"start_day\":1,\"end_day\":\"26\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"08\",\"last_updated_end_month\":\"08\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"26\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(17, 8, -22, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"07\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"07\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(18, 25, -24, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"07\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"07\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":18}'),
(19, 25, 0, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"12\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"12\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":19}'),
(20, 25, -23, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"13\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"13\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(21, 25, -25, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"15\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"15\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":21}'),
(22, 25, -11, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"22\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"22\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(23, 25, -10, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"22\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"22\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}'),
(24, 25, -20, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"09\",\"end_month\":\"09\",\"start_day\":1,\"end_day\":\"22\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"09\",\"last_updated_end_month\":\"09\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"22\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[],\"_source_query_id\":24}'),
(25, 25, -29, 0, '', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"10\",\"end_month\":\"10\",\"start_day\":1,\"end_day\":\"04\",\"start_year\":\"2022\",\"end_year\":\"2022\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"10\",\"last_updated_end_month\":\"10\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"04\",\"last_updated_start_year\":\"2022\",\"last_updated_end_year\":\"2022\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":[]}');

-- --------------------------------------------------------

--
-- Structure de la table `news_table`
--

CREATE TABLE `news_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `poster_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `announcement` tinyint(4) NOT NULL DEFAULT 0,
  `headline` varchar(64) NOT NULL DEFAULT '',
  `body` longtext NOT NULL,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_posted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `plugin_table`
--

CREATE TABLE `plugin_table` (
  `basename` varchar(40) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `priority` int(10) UNSIGNED NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `plugin_table`
--

INSERT INTO `plugin_table` (`basename`, `enabled`, `protected`, `priority`) VALUES
('Gravatar', 1, 0, 3),
('MantisCoreFormatting', 1, 0, 3),
('MantisGraph', 1, 0, 3),
('XmlImportExport', 1, 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `project_file_table`
--

CREATE TABLE `project_file_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `project_hierarchy_table`
--

CREATE TABLE `project_hierarchy_table` (
  `child_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `inherit_parent` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `project_table`
--

CREATE TABLE `project_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` smallint(6) NOT NULL DEFAULT 10,
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `access_min` smallint(6) NOT NULL DEFAULT 10,
  `file_path` varchar(250) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `inherit_global` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `project_user_list_table`
--

CREATE TABLE `project_user_list_table` (
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `project_version_table`
--

CREATE TABLE `project_version_table` (
  `id` int(11) NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(64) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `released` tinyint(4) NOT NULL DEFAULT 1,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0,
  `date_order` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sponsorship_table`
--

CREATE TABLE `sponsorship_table` (
  `id` int(11) NOT NULL,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `amount` int(11) NOT NULL DEFAULT 0,
  `logo` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT '',
  `paid` tinyint(4) NOT NULL DEFAULT 0,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `last_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tag_table`
--

CREATE TABLE `tag_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tokens_table`
--

CREATE TABLE `tokens_table` (
  `id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `expiry` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tokens_table`
--

INSERT INTO `tokens_table` (`id`, `owner`, `type`, `value`, `timestamp`, `expiry`) VALUES
(1, 1, 4, '1', 1666108876, 1666109176);

-- --------------------------------------------------------

--
-- Structure de la table `user_pref_table`
--

CREATE TABLE `user_pref_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `default_profile` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `default_project` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `refresh_delay` int(11) NOT NULL DEFAULT 0,
  `redirect_delay` int(11) NOT NULL DEFAULT 0,
  `bugnote_order` varchar(4) NOT NULL DEFAULT 'ASC',
  `email_on_new` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_assigned` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_feedback` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_closed` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_reopened` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_bugnote` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_status` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_status_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_bugnote_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_reopened_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_closed_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_resolved_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_feedback_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_assigned_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_new_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_bugnote_limit` smallint(6) NOT NULL DEFAULT 0,
  `language` varchar(32) NOT NULL DEFAULT 'english',
  `timezone` varchar(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_print_pref_table`
--

CREATE TABLE `user_print_pref_table` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `print_pref` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_profile_table`
--

CREATE TABLE `user_profile_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `platform` varchar(32) NOT NULL DEFAULT '',
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_table`
--

CREATE TABLE `user_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) NOT NULL DEFAULT '',
  `realname` varchar(191) NOT NULL DEFAULT '',
  `email` varchar(191) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `contact` varchar(64) DEFAULT NULL COMMENT 'numero_de_telephone',
  `numero_whatsapp` varchar(64) DEFAULT NULL COMMENT 'numero_whatsapp',
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10,
  `login_count` int(11) NOT NULL DEFAULT 0,
  `lost_password_request_count` smallint(6) NOT NULL DEFAULT 0,
  `failed_login_count` smallint(6) NOT NULL DEFAULT 0,
  `cookie_string` varchar(64) NOT NULL DEFAULT '',
  `last_visit` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user_table`
--

INSERT INTO `user_table` (`id`, `username`, `realname`, `email`, `password`, `contact`, `numero_whatsapp`, `enabled`, `protected`, `access_level`, `login_count`, `lost_password_request_count`, `failed_login_count`, `cookie_string`, `last_visit`, `date_created`) VALUES
(1, 'administrator', 'Administrateur Suivi Projet EBT', 'wkouakou@ebenyx.com', '200ceb26807d6bf99fd6f4f0d1ca54d4', NULL, NULL, 1, 0, 90, 165, 0, 0, '1JfdV_2Jjxq6yNM148pYdIQzZ23e7d8g9g-R6JQ11z_V4jXPfdUsyMT7c5t6PNE-', 1666108876, 1634845312);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `api_token_table`
--
ALTER TABLE `api_token_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_id_name` (`user_id`,`name`);

--
-- Index pour la table `app_config_table`
--
ALTER TABLE `app_config_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bugnote_table`
--
ALTER TABLE `bugnote_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug` (`bug_id`),
  ADD KEY `idx_last_mod` (`last_modified`);

--
-- Index pour la table `bugnote_text_table`
--
ALTER TABLE `bugnote_text_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bug_file_table`
--
ALTER TABLE `bug_file_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_file_bug_id` (`bug_id`),
  ADD KEY `idx_diskfile` (`diskfile`);

--
-- Index pour la table `bug_history_table`
--
ALTER TABLE `bug_history_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_history_bug_id` (`bug_id`),
  ADD KEY `idx_history_user_id` (`user_id`),
  ADD KEY `idx_bug_history_date_modified` (`date_modified`);

--
-- Index pour la table `bug_monitor_table`
--
ALTER TABLE `bug_monitor_table`
  ADD PRIMARY KEY (`user_id`,`bug_id`),
  ADD KEY `idx_bug_id` (`bug_id`);

--
-- Index pour la table `bug_relationship_table`
--
ALTER TABLE `bug_relationship_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_relationship_source` (`source_bug_id`),
  ADD KEY `idx_relationship_destination` (`destination_bug_id`);

--
-- Index pour la table `bug_revision_table`
--
ALTER TABLE `bug_revision_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_rev_type` (`type`),
  ADD KEY `idx_bug_rev_id_time` (`bug_id`,`timestamp`);

--
-- Index pour la table `bug_sms_table`
--
ALTER TABLE `bug_sms_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bug_table`
--
ALTER TABLE `bug_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_sponsorship_total` (`sponsorship_total`),
  ADD KEY `idx_bug_fixed_in_version` (`fixed_in_version`),
  ADD KEY `idx_bug_status` (`status`),
  ADD KEY `idx_project` (`project_id`);

--
-- Index pour la table `bug_tag_table`
--
ALTER TABLE `bug_tag_table`
  ADD PRIMARY KEY (`bug_id`,`tag_id`),
  ADD KEY `idx_bug_tag_tag_id` (`tag_id`);

--
-- Index pour la table `bug_text_table`
--
ALTER TABLE `bug_text_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category_table`
--
ALTER TABLE `category_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_category_project_name` (`project_id`,`name`);

--
-- Index pour la table `config_table`
--
ALTER TABLE `config_table`
  ADD PRIMARY KEY (`config_id`,`project_id`,`user_id`);

--
-- Index pour la table `custom_field_project_table`
--
ALTER TABLE `custom_field_project_table`
  ADD PRIMARY KEY (`field_id`,`project_id`);

--
-- Index pour la table `custom_field_string_table`
--
ALTER TABLE `custom_field_string_table`
  ADD PRIMARY KEY (`field_id`,`bug_id`),
  ADD KEY `idx_custom_field_bug` (`bug_id`);

--
-- Index pour la table `custom_field_table`
--
ALTER TABLE `custom_field_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_custom_field_name` (`name`);

--
-- Index pour la table `email_table`
--
ALTER TABLE `email_table`
  ADD PRIMARY KEY (`email_id`);

--
-- Index pour la table `filters_table`
--
ALTER TABLE `filters_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `news_table`
--
ALTER TABLE `news_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plugin_table`
--
ALTER TABLE `plugin_table`
  ADD PRIMARY KEY (`basename`);

--
-- Index pour la table `project_file_table`
--
ALTER TABLE `project_file_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `project_hierarchy_table`
--
ALTER TABLE `project_hierarchy_table`
  ADD UNIQUE KEY `idx_project_hierarchy` (`child_id`,`parent_id`),
  ADD KEY `idx_project_hierarchy_child_id` (`child_id`),
  ADD KEY `idx_project_hierarchy_parent_id` (`parent_id`);

--
-- Index pour la table `project_table`
--
ALTER TABLE `project_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_project_name` (`name`),
  ADD KEY `idx_project_view` (`view_state`);

--
-- Index pour la table `project_user_list_table`
--
ALTER TABLE `project_user_list_table`
  ADD PRIMARY KEY (`project_id`,`user_id`),
  ADD KEY `idx_project_user` (`user_id`);

--
-- Index pour la table `project_version_table`
--
ALTER TABLE `project_version_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_project_version` (`project_id`,`version`);

--
-- Index pour la table `sponsorship_table`
--
ALTER TABLE `sponsorship_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_sponsorship_bug_id` (`bug_id`),
  ADD KEY `idx_sponsorship_user_id` (`user_id`);

--
-- Index pour la table `tag_table`
--
ALTER TABLE `tag_table`
  ADD PRIMARY KEY (`id`,`name`),
  ADD KEY `idx_tag_name` (`name`);

--
-- Index pour la table `tokens_table`
--
ALTER TABLE `tokens_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_typeowner` (`type`,`owner`);

--
-- Index pour la table `user_pref_table`
--
ALTER TABLE `user_pref_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_print_pref_table`
--
ALTER TABLE `user_print_pref_table`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `user_profile_table`
--
ALTER TABLE `user_profile_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_cookie_string` (`cookie_string`),
  ADD UNIQUE KEY `idx_user_username` (`username`),
  ADD KEY `idx_enable` (`enabled`),
  ADD KEY `idx_access` (`access_level`),
  ADD KEY `idx_email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `api_token_table`
--
ALTER TABLE `api_token_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `app_config_table`
--
ALTER TABLE `app_config_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `bugnote_table`
--
ALTER TABLE `bugnote_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bugnote_text_table`
--
ALTER TABLE `bugnote_text_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_file_table`
--
ALTER TABLE `bug_file_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_history_table`
--
ALTER TABLE `bug_history_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_relationship_table`
--
ALTER TABLE `bug_relationship_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_revision_table`
--
ALTER TABLE `bug_revision_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_sms_table`
--
ALTER TABLE `bug_sms_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_table`
--
ALTER TABLE `bug_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bug_text_table`
--
ALTER TABLE `bug_text_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `category_table`
--
ALTER TABLE `category_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `custom_field_table`
--
ALTER TABLE `custom_field_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `email_table`
--
ALTER TABLE `email_table`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `filters_table`
--
ALTER TABLE `filters_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `news_table`
--
ALTER TABLE `news_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `project_file_table`
--
ALTER TABLE `project_file_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `project_table`
--
ALTER TABLE `project_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `project_version_table`
--
ALTER TABLE `project_version_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sponsorship_table`
--
ALTER TABLE `sponsorship_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tag_table`
--
ALTER TABLE `tag_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tokens_table`
--
ALTER TABLE `tokens_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `user_pref_table`
--
ALTER TABLE `user_pref_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user_profile_table`
--
ALTER TABLE `user_profile_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user_table`
--
ALTER TABLE `user_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
