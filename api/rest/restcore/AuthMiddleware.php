<?php
# MantisBT - A PHP based bugtracking system

# MantisBT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# MantisBT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MantisBT.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A webservice interface to Mantis Bug Tracker
 *
 * @package MantisBT
 * @copyright Copyright MantisBT Team - mantisbt-dev@lists.sourceforge.net
 * @link http://www.mantisbt.org
 */

require_api( 'authentication_api.php' );
require_api( 'user_api.php' );

/**
 * A middleware class that handles authentication and authorization to access APIs.
 */
class AuthMiddleware {
	public function __invoke( \Slim\Http\Request $request, \Slim\Http\Response $response, callable $next ) {
		// ebenyx ...
		$path = $request->getUri()->getPath();
		if ($path != "users/login"){}
		// ebenyx ...

		$t_authorization_header = $request->getHeaderLine( HEADER_AUTHORIZATION );

		// ebenyx ...
		// var_export($t_authorization_header);
		// exit("AuthMiddleware");
		// ebenyx ...

		if( empty( $t_authorization_header ) ) {
			# L'en-tête d'autorisation étant vide, vérifiez si l'utilisateur est authentifié en vérifiant le cookie
			# Ce mode est utilisé lorsque javascript de l'interface utilisateur Web appelle l'API.
			if( auth_is_user_authenticated() ) {
				$t_username = user_get_username( auth_get_current_user_id() );
				$t_password = auth_get_current_user_cookie( /* auto-login-anonymous */ false );
				$t_login_method = LOGIN_METHOD_COOKIE;
			} else {
				$t_username = auth_anonymous_account();

				if( !auth_anonymous_enabled() || empty( $t_username ) ) {
					return $response->withStatus( HTTP_STATUS_UNAUTHORIZED, 'API token required' );
				}

				$t_login_method = LOGIN_METHOD_ANONYMOUS;
				$t_password = '';
			}
		} else {
			# TODO: ajouter un index sur le hachage du jeton pour la méthode ci-dessous

			# Gérer plusieurs en-têtes d'autorisation (par exemple Basic + token)
			$t_authorization_headers = explode(', ', $t_authorization_header);
			$t_user_id = false;
			$t_api_token  = '';

			# Recherchez le jeton parmi les différents en-têtes d'autorisation.
			foreach( $t_authorization_headers as $value ) {
				$t_user_id = api_token_get_user( $value );
				if( $t_user_id !== false ) {
					# Valid token found
					$t_api_token = $value;
					break;
				}
			}

			if( $t_user_id === false ) {
				return $response->withStatus( HTTP_STATUS_FORBIDDEN, 'API token not found' );
			}

			# utiliser un jeton api
			$t_login_method = LOGIN_METHOD_API_TOKEN;
			$t_password = $t_api_token;
			$t_username = user_get_username( $t_user_id );
		}

		if( mci_check_login( $t_username, $t_password ) === false ) {
			return $response->withStatus( HTTP_STATUS_FORBIDDEN, 'Access denied' );
		}

		# Maintenant que l'utilisateur est connecté, vérifiez s'il dispose du bon niveau d'accès pour accéder à l'API REST.
		# Ne traitez pas les appels d'interface utilisateur Web avec des cookies comme des appels d'API qui doivent être
		# désactivés pour certains niveaux d'accès.
		if( $t_login_method != LOGIN_METHOD_COOKIE && !mci_has_readonly_access() ) {
			return $response->withStatus( HTTP_STATUS_FORBIDDEN, 'Higher access level required for API access' );
		}

		$t_force_enable = $t_login_method == LOGIN_METHOD_COOKIE;
		return $next( $request->withAttribute( ATTRIBUTE_FORCE_API_ENABLED, $t_force_enable ), $response )->
			withHeader( HEADER_USERNAME, $t_username )->
			withHeader( HEADER_LOGIN_METHOD, $t_login_method );
	}
}
