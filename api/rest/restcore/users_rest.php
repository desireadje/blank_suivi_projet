<?php

/**
 * @var \Slim\App $g_app
 */
$g_app->group('/users', function() use ( $g_app ) {
    $g_app->post( '/login', 'rest_user_post_login' );

	$g_app->get( '/me', 'rest_user_get_me' );

	$g_app->post( '/', 'rest_user_create' );
	$g_app->post( '', 'rest_user_create' );

	$g_app->delete( '/{id}', 'rest_user_delete' );
	$g_app->delete( '/{id}/', 'rest_user_delete' );

	$g_app->put( '/{id}/reset', 'rest_user_reset_password' );
});


// rest_user_post_login ...
function rest_user_post_login( \Slim\Http\Request $p_request, \Slim\Http\Response $p_response, array $p_args ) {

    // $method = $p_request->getMethod();
    $t_headers = $p_request->getHeaders();
    // var_export($t_headers["HTTP_AUTHORIZATION"]);

    $t_body = $p_request->getParsedBody();

    if (empty($t_body))
        return $p_response->withStatus( HTTP_STATUS_BAD_REQUEST );

    if (!isset($t_body["login"]) || !isset($t_body["password"]))
        return $p_response->withStatus( HTTP_STATUS_BAD_REQUEST );

    $t_login = $t_body["login"];    $t_password = $t_body["password"];

    if (auth_attempt_script_login($t_login, $t_password) == false)
        return $p_response->withStatus( HTTP_STATUS_NO_CONTENT );

    $t_result = mci_user_get( auth_get_current_user_id() );

    $t_access_level = $t_result["access_level"]["id"];

    if (!($t_access_level > VIEWER))
        return $p_response->withStatus( HTTP_STATUS_UNAUTHORIZED );

		
    $t_tokens = api_token_get_all( $t_result["id"] );
	if (empty($t_tokens)) 
		return $p_response->withStatus( HTTP_STATUS_UNAUTHORIZED, 'API token required' );

	$t_token_0 = $t_tokens[0]["plain_token"];
	// ebenyx : set token
	$t_result["token"] = $t_token_0;
	// ebenyx : set token

	// var_export($t_result["token"]);
	// var_export($t_token_0);
    // exit;
	// return $response->withStatus( HTTP_STATUS_UNAUTHORIZED, 'API token required' );

	return $p_response->withStatus( HTTP_STATUS_SUCCESS )->withJson( $t_result );
}


/**
 * Une méthode qui fait le travail pour obtenir des informations sur l'utilisateur actuellement connecté.
 *
 * @param \Slim\Http\Request $p_request   The request.
 * @param \Slim\Http\Response $p_response The response.
 * @param array $p_args Arguments
 *
 * @return \Slim\Http\Response The augmented response.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function rest_user_get_me( \Slim\Http\Request $p_request, \Slim\Http\Response $p_response, array $p_args ) {

    $method = $p_request->getMethod();
    $uri = $p_request->getUri();
    $headers = $p_request->getHeaders();

    $_token_auth = $headers["HTTP_AUTHORIZATION"];
    // var_dump(_token_auth);

    $t_result = mci_user_get( auth_get_current_user_id() );
	return $p_response->withStatus( HTTP_STATUS_SUCCESS )->withJson( $t_result );
}

/**
 * A method that creates a user.
 *
 * @param \Slim\Http\Request $p_request   The request.
 * @param \Slim\Http\Response $p_response The response.
 * @param array $p_args Arguments
 *
 * @return \Slim\Http\Response The augmented response.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function rest_user_create( \Slim\Http\Request $p_request, \Slim\Http\Response $p_response, array $p_args ) {
	$t_payload = $p_request->getParsedBody();
	if( !$t_payload ) {
		return $p_response->withStatus( HTTP_STATUS_BAD_REQUEST, "Invalid request body or format");
	}

	$t_data = array( 'payload' => $t_payload );
	$t_command = new UserCreateCommand( $t_data );
	$t_result = $t_command->execute();
	$t_user_id = $t_result['id'];

	return $p_response->withStatus( HTTP_STATUS_CREATED, "User created with id $t_user_id" )->
		withJson( array( 'user' => mci_user_get( $t_user_id ) ) );
}

/**
 * Delete a user given its id.
 *
 * @param \Slim\Http\Request $p_request   The request.
 * @param \Slim\Http\Response $p_response The response.
 * @param array $p_args Arguments
 *
 * @return \Slim\Http\Response The augmented response.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function rest_user_delete( \Slim\Http\Request $p_request, \Slim\Http\Response $p_response, array $p_args ) {
	$t_user_id = $p_args['id'];

	$t_data = array(
		'query' => array( 'id' => $t_user_id )
	);

	$t_command = new UserDeleteCommand( $t_data );
	$t_command->execute();

	return $p_response->withStatus( HTTP_STATUS_NO_CONTENT );
}

/**
 * Reset a user's password given its id.
 *
 * @param \Slim\Http\Request $p_request   The request.
 * @param \Slim\Http\Response $p_response The response.
 * @param array $p_args Arguments
 *
 * @return \Slim\Http\Response The augmented response.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function rest_user_reset_password( \Slim\Http\Request $p_request, \Slim\Http\Response $p_response, array $p_args ) {
	$t_user_id = $p_args['id'];

	$t_data = array(
		'query' => array( 'id' => $t_user_id )
	);

	$t_command = new UserResetPasswordCommand( $t_data );
	$t_command->execute();

	return $p_response->withStatus( HTTP_STATUS_NO_CONTENT );
}
